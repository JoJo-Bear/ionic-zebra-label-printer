# Plugin information
This plugin will enable your Ionic Cordova project to print using a Zebra printer. It's written to use TCP sockets for the native Android and iOS. In a browser version it will use a post request. If you are planning to use it in a web version served from HTTPS be warned, the Zebra printer only allows http calls. This causes Mixed Content issues if your not able to update the browser flag on the devices that will be used this will be a mayor issue. 

## Usage
You need to import the plugin in the file in which you want to call your print actions: 
```typescript
import { Plugins } from '@capacitor/core';

import 'ionic-zebra-label-printer';
const { ZebraLabelPrinter } = Plugins;

```

Initialise the printer object by passing the ip of the printer, port on which it is accessible (default for zebra is 9100) and code you want to print. \
Use the print object within the call to start printing.
```typescript
let PrintObj = {
			ip: "xxx.xxx.xxx.xxx",
			port: xxxx,
			zpl: "your zpl formatted string"
		};

await ZebraLabelPrinter.print(PrintObj)
		.then(
			(res)=>{
			},
			(err)=>{
			}
		).finally(()=>{
		});

```


## Android
##### This version is tested on physical devises (zebra scanner to printer and android phone to printer)
This requires some manual steps to use this plugin on android 

Within the main activity you need to import the plugin and add it within the init function
```java
    import nl.appcomm.zebraLabelPrinter.ZebraLabelPrinter; // <-- add this line

   	// Initializes the Bridge
    this.init(savedInstanceState, new ArrayList<Class<? extends Plugin>>() {{
      // Additional plugins you've installed go here
      // Ex: add(TotallyAwesomePlugin.class);
      add(ZebraLabelPrinter.class); // <-- add this line
    }});
```

## iOS

The iOS version is written with the use of the SwiftSocket pod. After building your project it could complain about;
- the deployment target
- the swift version

Both are easily solvable

### Deployment target
While having xcode open press on the Pods.xcodeproj > SwiftSocket (whithin the Targets section) > General (tab) > Deployment Info 
Here you select the target needed.

### Swift version
While having xcode open press on the Pods.xcodeproj > SwiftSocket (whithin the Targets section) > Build Settings (tab) > Swift Language Version
Here you select the needed version

## Web

The web version of this plugin uses an alternative method.
It uses the post request option provided by zebra.
Howver in order to uses this option you will need to enable a browser flag.
This approach differs for each browser. In order to alter the flag you will need to search for "allow mixed content" 