import { WebPlugin } from '@capacitor/core';
import { ZebraLabelPrinterPlugin } from './definitions';

export class ZebraLabelPrinterWeb extends WebPlugin implements ZebraLabelPrinterPlugin {
  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }

  async print(options: {ip: string, port: number, zpl: string }): Promise<{ value:string }> {

    var request = new Request("http://"+options.ip+"/pstprnt",{
      method: 'POST',
      mode: 'no-cors',
      cache: 'no-cache',
      body: options.zpl
    });
    return await fetch(request)
    .then(()=>{
      return {value: "print succesfully executed" }
    })
    .catch((error)=>{
        throw Error(error);
    });
  }
}

const ZebraLabelPrinter = new ZebraLabelPrinterWeb();

export { ZebraLabelPrinter };