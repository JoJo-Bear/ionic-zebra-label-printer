import { registerPlugin } from '@capacitor/core';
import type { ZebraLabelPrinterPlugin } from './definitions';

const ZebraLabelPrinter = registerPlugin<ZebraLabelPrinterPlugin>('ZebraLabelPrinter', {
  web: () => import('./web').then(m => new m.ZebraLabelPrinterWeb()),
});

export * from './definitions';
export { ZebraLabelPrinter };

