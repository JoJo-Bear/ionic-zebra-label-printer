package nl.appcomm.zebraLabelPrinter;

import com.getcapacitor.JSObject;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

@CapacitorPlugin(name = "ZebraLabelPrinter")
public class ZebraLabelPrinter extends Plugin {

    @PluginMethod
    public void echo(PluginCall call) {
        String value = call.getString("value");

        JSObject ret = new JSObject();
        ret.put("value", value);
        call.resolve(ret);
    }

    @PluginMethod
    public void print(PluginCall call) {
        //getting the values from the app
        String ip = call.getString("ip");
        int port = call.getInt("port");
        String zpl = call.getString("zpl");

        //Do the magic native print commands
        System.out.println("start");
        System.out.println("------------------------------------------------------------");

        // String zpl = "^XA\r\n" +
        // 		"^FO50,50\r\n" +
        // 		"^B8N,100,Y,N\r\n" +
        // 		"^FD1234567^FS\r\n" +
        // 		"^XZ";
        // String ip = "192.168.17.239"; // PRT-LH-03

        // int port = 9100;

        Boolean error = false;
        Socket clientSocket = null;
        DataOutputStream outToServer = null;
        try {
            System.out.println("Printing now...");
            clientSocket = new Socket(ip, port);
            outToServer = new DataOutputStream(clientSocket.getOutputStream());
            outToServer.writeBytes(zpl);
            outToServer.close();
            System.out.println("send the following string: " + zpl);
            clientSocket.close();
        } catch (Exception exception) {
            System.out.println("Cannot print label on this printer : " + ip + ":" + port);
            System.out.println(exception.getMessage());
            call.reject(exception.getMessage());
            error = true;
        } finally {
            if (outToServer != null) {
                try {
                    outToServer.close();
                } catch (IOException e) {
                    System.out.println("Failed to close DataOutputStream: " + e.getMessage());
                }
            }
            if (clientSocket != null) {
                try {
                    clientSocket.close();
                } catch (IOException e) {
                    System.out.println("Failed to close Socket: " + e.getMessage());
                }
            }
        }

        System.out.println("------------------------------------------------------------");
        System.out.println("end");

        if (!error) {
            // send a response back
            JSObject ret = new JSObject();
            ret.put("value", "Succesfully sent to printer");
            call.resolve(ret);
        }
    }
}
